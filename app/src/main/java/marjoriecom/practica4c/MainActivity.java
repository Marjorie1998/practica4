package marjoriecom.practica4c;

import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View .OnClickListener,
fragmentouno.OnFragmentInteractionListener, fragmentodos.OnFragmentInteractionListener {
    Button btnu, btnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnu = (Button) findViewById(R.id.btnu);
        btnd = (Button) findViewById(R.id.btnd);
        btnu.setOnClickListener(this);
        btnd.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnu:
                fragmentouno fragmentoUno =new fragmentouno();
                FragmentTransaction transactionUno = getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor, fragmentoUno);
                transactionUno.commit();
                break;
            case R.id.btnd:
                fragmentodos fragmentoDos =new fragmentodos();
                FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor, fragmentoDos);
                transactionDos.commit();
                break;
        }
}

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
